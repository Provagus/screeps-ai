var behaviourEnergy = require('behaviour.energy');

var roleUpgrader = {

    /** @param {Creep} creep **/
    run: function(creep) {
        if(creep.memory.full && creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0) {
            creep.memory.full = false;
            creep.say('🔄');
        }
        if(!creep.memory.full && creep.store.getUsedCapacity(RESOURCE_ENERGY) == creep.store.getCapacity(RESOURCE_ENERGY)) {
            creep.memory.full = true;
            creep.say('⚡');
            behaviourEnergy.gatheredEnergy(creep);
        }

        if(creep.memory.full) {
            if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller, {visualizePathStyle: {stroke: '#ffffff', opacity: 0.9}});
            }
        }
        else {
            behaviourEnergy.gatherEnergy(creep);
        }
    }
};

module.exports = roleUpgrader;