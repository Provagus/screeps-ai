require('prototype.room.planner')();
const { HARVESTER, RUNNER, DEFENDER, MINER, UPGRADER, FIXER, RAMPART, BUILDER, FRIENDS, LONG_DISTANCE_HARVESTER, FUEL_TRUCK } = require("./constants");
const SpawnManager = require("./manager.spawn");
const behaviourTower = require('behaviour.tower');
const { filter } = require("lodash");
const HARVESTER_PRIORITY = 5;
const UPGRADER_PRIORITY = 1;

module.exports = function() {
    Room.prototype.PerformNormalTasks = function(){
        //Looking for hostiles
        let hostiles = this.find(FIND_HOSTILE_CREEPS, {
            filter: (creep) => !(creep.owner.username in FRIENDS)
        });

        //Checking if room is mine
        if(!this.controller || !this.controller.my){
            return;
        }

        //Regular checking if new ramparts needed
        if(Game.time % 25 == 0){
            this.SecureVulnerabilities(STRUCTURE_CONTAINER, false);
            this.SecureVulnerabilities(STRUCTURE_SPAWN);
            this.SecureVulnerabilities(STRUCTURE_STORAGE);
            this.SecureVulnerabilities(STRUCTURE_EXTENSION);
            this.SecureVulnerabilities(STRUCTURE_TOWER);
            this.ConstructExtensions();
        }
        if(Game.time % 50 == 0){
            this.FindJoinedRooms();
            this.ConstructStorages();
            this.ConstructPathToStorages();
            this.ConstructPathToController();
        }
        //this.planRoom();

        //Counting creeps assigned to the room
        let roomCreeps = {};
        let roomRoleCount = {};
        let nearestDeath = 10000;
        let nearestDeathName = "";

        let roomAssignedCreeps = _.filter(Game.creeps, (creep) => creep.memory.home == this.name);
        for(let creep in roomAssignedCreeps){
            if (roomAssignedCreeps[creep].ticksToLive < nearestDeath){
                nearestDeath = roomAssignedCreeps[creep].ticksToLive;
                nearestDeathName = roomAssignedCreeps[creep].name;
            }
        }

        for(let role in global.roles){
            roomCreeps[role] = _.filter(roomAssignedCreeps, (creep) => creep.memory.role == role && creep.memory.home == this.name);
            if(role != LONG_DISTANCE_HARVESTER){
                roomRoleCount[role] = roomCreeps[role].length;
                if(!("waitingForSpawn" in this.memory) ||
                    !(role in this.memory.waitingForSpawn)){
                    continue;
                }
                roomRoleCount[role] += this.memory.waitingForSpawn[role];
            }
        }
        let harvestRooms = this.memory.connectedRooms;
        if(harvestRooms === undefined){
            harvestRooms = [];
        }
        roomRoleCount[LONG_DISTANCE_HARVESTER] = {};
        for(let harvestRoom in harvestRooms){
            harvestRoom = harvestRooms[harvestRoom];
            roomRoleCount[LONG_DISTANCE_HARVESTER][harvestRoom] = _.filter(roomCreeps[LONG_DISTANCE_HARVESTER], 
                creep => creep.memory.harvestRoom == harvestRoom).length;
            if(!("waitingForSpawn" in this.memory) ||
                !(LONG_DISTANCE_HARVESTER in this.memory.waitingForSpawn) ||
                !(harvestRoom in this.memory.waitingForSpawn[LONG_DISTANCE_HARVESTER])){
                continue;
            }
            roomRoleCount[LONG_DISTANCE_HARVESTER][harvestRoom] += this.memory.waitingForSpawn[LONG_DISTANCE_HARVESTER][harvestRoom];
        }

        let roomTowers = this.find(FIND_MY_STRUCTURES, {
            filter: object => object.structureType == STRUCTURE_TOWER
        });

        let continueAfterHarvestRooms = false;

        //Decisions what to spawn next
        if(roomCreeps[HARVESTER] == 0 && roomCreeps[RUNNER].length == 0 && roomCreeps[MINER].length == 0){
            SpawnManager.createCustomCreep(Math.max(this.energyAvailable, 300), HARVESTER, this, Number.MAX_SAFE_INTEGER);
        }

        /*else if((roomCreeps[DEFENDER].length < 1 && this.find(FIND_MY_STRUCTURES, {
            filter: object => object.structureType == STRUCTURE_TOWER
        }).length == 0) || hostiles.length > (roomCreeps[DEFENDER].length * 2)){
            var newName = 'Defender' + Game.time;
            spawn.spawnCreep(attackCreep, newName,
                {memory: {role: DEFENDER}});
        }*/

        else if(roomRoleCount[MINER] < this.find(FIND_SOURCES).length && 
            this.find(FIND_STRUCTURES, {
                filter: object => object.structureType == STRUCTURE_CONTAINER
            }).length > roomRoleCount[MINER]){
            // check if all sources have miners
            let sources = this.find(FIND_SOURCES);
            // iterate over all sources
            for (let source of sources) {
                // check whether or not the source has a container
                /** @type {Array.StructureContainer} */
                let containers = source.pos.findInRange(FIND_STRUCTURES, 1, {
                    filter: s => s.structureType == STRUCTURE_CONTAINER
                });
                // if there is a container next to the source
                if (containers.length > 0) {
                    // spawn a miner
                    SpawnManager.createMiner(Math.min(1300, this.energyCapacityAvailable), this);
                    break;
                }
            }
        }

        else if(roomRoleCount[MINER] > 0 && roomRoleCount[RUNNER] < 2){
            SpawnManager.createRunner(Math.min(2000, this.energyCapacityAvailable), this);
        }
        
        else if(roomRoleCount[HARVESTER] < 3 && roomRoleCount[RUNNER] == 0) {
            SpawnManager.createCustomCreep(Math.min(1300, this.energyCapacityAvailable), HARVESTER, this, HARVESTER_PRIORITY);
        }
        
        else if(roomRoleCount[UPGRADER] < 1) {
            SpawnManager.createCustomCreep(Math.min(1300, this.energyCapacityAvailable), UPGRADER, this, UPGRADER_PRIORITY);
        }

        else if(_.filter(roomTowers, (tower) => tower.store.getFreeCapacity(RESOURCE_ENERGY) > 0
        ).length > 0 && roomRoleCount[FUEL_TRUCK] < 1){
            SpawnManager.createCustomCreep(Math.min(1300, this.energyCapacityAvailable), FUEL_TRUCK, this);
        }

        else if(roomRoleCount[FIXER] < 1 && _.filter(roomTowers, (tower) =>
            tower.hits == tower.hitsMax
        ).length == 0) {
            SpawnManager.createCustomCreep(Math.min(1300, this.energyCapacityAvailable), FIXER, this);
        }
        
        else if(roomRoleCount[RAMPART] < 1) {
            SpawnManager.createCustomCreep(Math.min(this.energyCapacityAvailable, 1300), RAMPART, this);
        }
        
        else if(roomRoleCount[BUILDER] < 2 && this.find(FIND_CONSTRUCTION_SITES, {
            filter: s => s.structureType != STRUCTURE_RAMPART && s.structureType != STRUCTURE_WALL
        }).length > 0) {
            SpawnManager.createCustomCreep(Math.min(1300, this.energyCapacityAvailable), BUILDER, this);
        }

        else{
            for (let i = 0; i < harvestRooms.length; i++){
                let harvestRoomName = harvestRooms[i];
                if(roomRoleCount[LONG_DISTANCE_HARVESTER][harvestRoomName] < 2) {
                    SpawnManager.createLongDistanceHarvesterCreep(Math.min(1300, this.energyCapacityAvailable),
                        LONG_DISTANCE_HARVESTER, harvestRoomName, this);
                    continueAfterHarvestRooms = true;
                    break;
                }
            }
        }

        if (continueAfterHarvestRooms === false){}
        else if(roomRoleCount[UPGRADER] < 2){
            SpawnManager.createCustomCreep(Math.min(1300, this.energyCapacityAvailable), UPGRADER, this);
        }

        //Tower behaviour
        for(let name in roomTowers){
            let tower = roomTowers[name];
            behaviourTower.run(tower);
        }

        //Border status
        //Memory status
        let borderStatus = this.memory.borderStatus;
        if(typeof borderStatus == "undefined"){
            this.memory.borderStatus = false;
            borderStatus = false;
        }

        //New rampart handling
        if(this.memory.newStructure){
            this.memory.newStructure = false;
            let ramparts = this.find(FIND_MY_STRUCTURES, {
                filter: object => object.structureType == STRUCTURE_RAMPART
            });
            for(let rampartStructure in ramparts){
                ramparts[rampartStructure].setPublic(!this.memory.borderStatus);
            }
        }

        if(hostiles.length == 0 && borderStatus == true){
            this.memory.borderStatus = false;
            let ramparts = this.find(FIND_MY_STRUCTURES, {
                filter: object => object.structureType == STRUCTURE_RAMPART
            });
            console.log("Open gates");
            for(let rampartStructure in ramparts){
                ramparts[rampartStructure].setPublic(true);
            }
        }
        else if(hostiles.length > 0 && borderStatus == false){
            this.memory.borderStatus = true;
            let ramparts = this.find(FIND_MY_STRUCTURES, {
                filter: object => object.structureType == STRUCTURE_RAMPART
            });
            console.log("Close gates");
            for(let rampartStructure in ramparts){
                ramparts[rampartStructure].setPublic(false);
            }
        }

        if(Memory.overlay){
            let hght = 0;
            this.visual.text(
                "STATUS",
                0, hght,
                {align: 'left', opacity: 0.8});
            hght++;
            this.visual.text(
                "Nearest death: " + nearestDeathName + " - " + nearestDeath,
                0, hght,
                {align: 'left', opacity: 0.8});
            hght++;
            for(let role in global.roles){
                if(role == LONG_DISTANCE_HARVESTER){
                    for(let room in harvestRooms){
                        let amount = _.filter(roomCreeps[LONG_DISTANCE_HARVESTER], (creep) => creep.memory.harvestRoom == harvestRooms[room]).length;
                        this.visual.text(
                            "Role " + LONG_DISTANCE_HARVESTER + " - " + harvestRooms[room] + ": " + amount,
                            0, hght,
                            {align: 'left', opacity: 0.8});
                        hght++;
                    }
                }
                else{
                    this.visual.text(
                        "Role " + role + ": " + roomCreeps[role].length,
                        0, hght,
                        {align: 'left', opacity: 0.8});
                    hght++;
                }
            }
            this.visual.text(
                "Spawn energy: " + this.energyAvailable + " / " + this.energyCapacityAvailable,
                0, hght,
                {align: 'left', opacity: 0.8});
            hght++;
            this.visual.text(
                Memory.borderStatus ? "Gates closed" : "Gates open",
                0, hght,
                {align: 'left', opacity: 0.8, color: Memory.borderStatus ? "#ff0000" : "#00ff00"});
            hght++;
        }
    };

    Room.prototype.SecureVulnerabilities =
        function (structure, playerOwner=true) {
            let containers = this.find(playerOwner ? FIND_MY_STRUCTURES : FIND_STRUCTURES, {
                filter: (s) => s.structureType == structure
            });
            containers.forEach(cnt => {
                let found = cnt.room.lookForAt(LOOK_STRUCTURES, cnt.pos);
                let foundRampart = false;
                for(let structure in found){
                    if(found[structure].structureType == STRUCTURE_RAMPART){
                        foundRampart = true;
                        break;
                    }
                }
                if(foundRampart){
                    return;
                }
                found = cnt.room.lookForAt(LOOK_CONSTRUCTION_SITES, cnt.pos);
                for(let structure in found){
                    if(found[structure].structureType == STRUCTURE_RAMPART){
                        foundRampart = true;
                        break;
                    }
                }
                if(!foundRampart){
                    cnt.room.createConstructionSite(cnt.pos, STRUCTURE_RAMPART);
                }
            });
        };

    Room.prototype.FindJoinedRooms =
        function () {
            let exits = Game.map.describeExits(this.name);
            let connectedRooms = [];
            let roomStatus = Game.map.getRoomStatus(this.name).status;
            for(let exit in exits){
                if (Game.map.getRoomStatus(exits[exit]).status === roomStatus){
                    connectedRooms.push(exits[exit]);
                }
            }
            this.memory.connectedRooms = connectedRooms;
        }
    
    Room.prototype.ConstructExtensions =
        function () {
            let availableExtensions = CONTROLLER_STRUCTURES[STRUCTURE_EXTENSION][this.controller.level];
            let extensions = this.find(FIND_MY_STRUCTURES, {
                filter: (s) => s.structureType == STRUCTURE_EXTENSION
            });
            let beingBuilt = this.find(FIND_MY_CONSTRUCTION_SITES, {
                filter: (s) => s.structureType == STRUCTURE_EXTENSION
            });
            if (extensions.length + beingBuilt.length >= availableExtensions){
                return;
            }
            let spawn = this.find(FIND_MY_SPAWNS);
            if(spawn.length > 0){
                spawn = spawn[0];
            }
            let delta = availableExtensions - extensions.length - beingBuilt.length;
            let even = (spawn.pos.x + spawn.pos.y) % 2;
            let dist = 1;
            let keepGoing = true;
            while(delta > 0 && keepGoing){
                for(let i = spawn.pos.x - dist; i <= spawn.pos.x + dist && keepGoing; i++){
                    if(i <= 0 || i >= 49){
                        continue;
                    }
                    for(let j = spawn.pos.y - dist; j <= spawn.pos.y + dist && keepGoing; j++){
                        if(j <= 0 || j >= 49){
                            continue;
                        }
                        if(Math.abs(spawn.pos.y - j) !== dist && Math.abs(spawn.pos.x - i) !== dist){
                            continue;
                        }
                        
                        if(this.getTerrain().get(i, j) == TERRAIN_MASK_WALL){
                            continue;
                        }
                        let atPos = this.lookAt(i,j);
                        let valid = true;
                        for(let elem in atPos){
                            let element = atPos[elem];
                            if(element.type === "constructionSite" && element.constructionSite.structureType !== STRUCTURE_RAMPART){
                                valid = false;
                                break;
                            }
                            if(element.type === "structure" && element.structure.structureType !== STRUCTURE_RAMPART){
                                valid = false;
                                break;
                            }
                        }
                        if(!valid){
                            continue;
                        }
                        if((i + j) % 2 == even){
                            this.createConstructionSite(i, j, STRUCTURE_EXTENSION);
                            delta--;
                            if(delta === 0){
                                keepGoing = false;
                                break;
                            }
                        }
                        else{
                            this.createConstructionSite(i, j, STRUCTURE_ROAD);
                        }
                    }
                }
                dist++;
            }
        }

    Room.prototype.ConstructStorages =
        function () {
            let sources = this.find(FIND_SOURCES);
            for(let sourceName in sources){
                let source = sources[sourceName];
                let connectedContainers = source.pos.findInRange(FIND_STRUCTURES, 1, {
                    filter: (s) => s.structureType == STRUCTURE_CONTAINER
                }).length;
                connectedContainers += source.pos.findInRange(FIND_CONSTRUCTION_SITES, 1, {
                    filter: (s) => s.structureType == STRUCTURE_CONTAINER
                }).length;
                if(connectedContainers != 0){
                    continue;
                }
                let path = source.pos.findPathTo(this.find(FIND_MY_SPAWNS)[0], {ignoreCreeps: true});
                if(path.length == 0){
                    continue;
                }
                this.createConstructionSite(path[0].x, path[0].y, STRUCTURE_CONTAINER);
            }
        }
    
    Room.prototype.ConstructPathToStorages =
        function () {
            let containers = this.find(FIND_STRUCTURES, {
                filter: (s) => s.structureType == STRUCTURE_CONTAINER
            });
            let containers2 = this.find(FIND_CONSTRUCTION_SITES, {
                filter: (s) => s.structureType == STRUCTURE_CONTAINER
            });
            containers = containers.concat(containers2);
            for(let containerName in containers){
                let container = containers[containerName];
                let path = container.pos.findPathTo(this.find(FIND_MY_SPAWNS)[0], {ignoreCreeps: true});
                if(path.length == 0){
                    continue;
                }
                for(let i = 0; i < path.length; i++){
                    let atPos = this.lookAt(path[i].x, path[i].y);
                    let valid = true;
                    for(let elem in atPos){
                        let element = atPos[elem];
                        if(element.type === "constructionSite" && element.constructionSite.structureType !== STRUCTURE_RAMPART){
                            valid = false;
                            break;
                        }
                        if(element.type === "structure" && element.structure.structureType !== STRUCTURE_RAMPART){
                            valid = false;
                            break;
                        }
                    }
                    if(valid){
                        this.createConstructionSite(path[i].x, path[i].y, STRUCTURE_ROAD);
                    }
                }
            }
        }
    Room.prototype.ConstructPathToController =
        function () {
            let controller = this.controller;
            let path = controller.pos.findPathTo(this.find(FIND_MY_SPAWNS)[0], {ignoreCreeps: true});
            if(path.length == 0){
                return;
            }
            for(let i = 0; i < path.length; i++){
                let atPos = this.lookAt(path[i].x, path[i].y);
                let valid = true;
                for(let elem in atPos){
                    let element = atPos[elem];
                    if(element.type === "constructionSite" && element.constructionSite.structureType !== STRUCTURE_RAMPART){
                        valid = false;
                        break;
                    }
                    if(element.type === "structure" && element.structure.structureType !== STRUCTURE_RAMPART){
                        valid = false;
                        break;
                    }
                }
                if(valid){
                    this.createConstructionSite(path[i].x, path[i].y, STRUCTURE_ROAD);
                }
            }
        }
}