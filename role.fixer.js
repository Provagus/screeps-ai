var roleBuilder = require('role.builder');
var behaviourEnergy = require('behaviour.energy');

var roleFixer = {

    /** @param {Creep} creep **/
    run: function(creep) {
	    if(creep.memory.full && creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0) {
            creep.memory.full = false;
            creep.say('🔄');
            creep.memory.assigned = false;
	    }
	    if(!creep.memory.full && creep.store.getUsedCapacity(RESOURCE_ENERGY) == creep.store.getCapacity(RESOURCE_ENERGY)) {
	        creep.memory.full = true;
	        creep.say('🛠');
	    }
        var canFix = true;
	    if(creep.memory.full) {
            if(Game.getObjectById(creep.memory.assignation) == null){
                creep.memory.assigned = false;
            }
            if(!creep.memory.assigned){
                var targets2 = creep.room.find(FIND_MY_STRUCTURES, {
                    filter: object => object.hits < object.hitsMax
                });
                var targets = creep.room.find(FIND_STRUCTURES, {
                    filter: object => (object.structureType == "constructedWall" || object.structureType == STRUCTURE_ROAD || object.structureType == STRUCTURE_CONTAINER) && object.hits < object.hitsMax
                });

                targets2 = targets2.concat(targets);
                
                targets2.sort((a,b) => a.hits == b.hits ? creep.pos.getRangeTo(a.pos) - creep.pos.getRangeTo(b.pos) : a.hits - b.hits);
                if(targets2.length > 0){
                    creep.memory.assignation = targets2[0].id;
                    creep.memory.assigned = true;
                }
                else{
                    canFix = false;
                }
            }
            if(creep.memory.assigned && Game.getObjectById(creep.memory.assignation).hits < Game.getObjectById(creep.memory.assignation).hitsMax) {
                if(creep.repair(Game.getObjectById(creep.memory.assignation)) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(Game.getObjectById(creep.memory.assignation), {visualizePathStyle: {stroke: '#ffff00'}});
                }
            }
            else{
                creep.memory.assigned = false;
            }
            if(!canFix){
                roleBuilder.run(creep);
            }
	    }
	    else {
	        behaviourEnergy.gatherEnergy(creep);
	    }
	}
};

module.exports = roleFixer;