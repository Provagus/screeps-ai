var behaviourTower = {

    /** @param {Tower} tower **/
    run: function(tower) {
        var closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS, {
            filter: (creep) => creep.owner.username != "BlkDrake"
        });
        if(closestHostile) {
            tower.attack(closestHostile);
        }
        else if(tower.store.getUsedCapacity(RESOURCE_ENERGY) / tower.store.getCapacity(RESOURCE_ENERGY) > 0.8){
            var closestToHeal = tower.pos.findClosestByRange(FIND_MY_CREEPS, {
                filter: (creep) => creep.hits < creep.hitsMax
            });
            if(!closestToHeal){
                closestToHeal = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS, {
                    filter: (creep) => creep.owner.username == "BlkDrake" && creep.hits < creep.hitsMax
                });
            }
            if(closestToHeal) {
                tower.heal(closestToHeal);
            }
            //console.log(tower.store.getUsedCapacity(RESOURCE_ENERGY) / tower.store.getCapacity(RESOURCE_ENERGY));
            else if(tower.store.getUsedCapacity(RESOURCE_ENERGY) / tower.store.getCapacity(RESOURCE_ENERGY) > 0.9){
                var damagedStructure = tower.room.find(FIND_STRUCTURES, {
                    filter: (structure) => structure.hits < structure.hitsMax && (structure.structureType != "constructedWall" && 
                        structure.structureType != STRUCTURE_RAMPART) || (structure.structureType == "constructedWall" && structure.hits <= 10000) ||
                        (structure.structureType == STRUCTURE_RAMPART && structure.hits <= 10000) 
                });
                damagedStructure.sort((a,b) => a.hits / a.hitsMax == b.hits / b.hitsMax ? 
                    tower.pos.getRangeTo(a.pos) - tower.pos.getRangeTo(b.pos) : a.hits / a.hitsMax - b.hits / b.hitsMax);
                if(damagedStructure.length > 0) {
                    tower.repair(damagedStructure[0]);
                }
                else if(tower.store.getUsedCapacity(RESOURCE_ENERGY) / tower.store.getCapacity(RESOURCE_ENERGY) > 0.95){
                    damagedStructure = tower.room.find(FIND_STRUCTURES, {
                        filter: (structure) => structure.hits < structure.hitsMax && (structure.structureType == "constructedWall" || 
                            structure.structureType == STRUCTURE_RAMPART) 
                    });
                    damagedStructure.sort((a,b) => a.hits == b.hits ? tower.pos.getRangeTo(a.pos) - tower.pos.getRangeTo(b.pos) : a.hits - b.hits);
                    if(damagedStructure.length > 0) {
                        tower.repair(damagedStructure[0]);
                    }
                }
            }
        }
    }
};

module.exports = behaviourTower;