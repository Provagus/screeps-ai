var behaviourHarvester = require('behaviour.harvester');
var behaviourEnergy = require('behaviour.energy');

var roleHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {
        if(creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0){
            creep.memory.full = false;
        }
        else if(creep.store.getUsedCapacity(RESOURCE_ENERGY) >= creep.store.getCapacity(RESOURCE_ENERGY) && creep.memory.full == false){
            creep.memory.full = true;
            creep.say('🚚');
            behaviourEnergy.gatheredEnergy(creep);
        }

	    if(creep.memory.full == false) {
            behaviourEnergy.gatherEnergyHarvester(creep);
        }
        else {
            behaviourHarvester.deploy(creep);
        }
	}
};

module.exports = roleHarvester;