var roleFixer = require('role.fixer');
var behaviourEnergy = require('behaviour.energy');

var roleRampart = {

    /** @param {Creep} creep **/
    run: function(creep) {
	    if(creep.memory.full && creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0) {
            creep.memory.builded = false;
            creep.memory.full = false;
            creep.say('🔄');
            creep.memory.assigned = false;
	    }
	    if(!creep.memory.full && creep.store.getUsedCapacity(RESOURCE_ENERGY) == creep.store.getCapacity(RESOURCE_ENERGY)) {
	        creep.memory.full = true;
            creep.say('🛠');
            behaviourEnergy.gatheredEnergy(creep);
	    }
        var canFix = true;
	    if(creep.memory.full) {
            if(Game.getObjectById(creep.memory.assignation) == null){
                creep.memory.assigned = false;
            }
            if(!creep.memory.assigned){
                var targets2 = creep.room.find(FIND_MY_STRUCTURES, {
                    filter: object => object.structureType == STRUCTURE_RAMPART && object.hits < object.hitsMax
                });
                var targets = creep.room.find(FIND_STRUCTURES, {
                    filter: object => object.structureType == STRUCTURE_WALL && object.hits < object.hitsMax
                });

                targets2 = targets2.concat(targets);

                targets2.sort((a,b) => a.hits == b.hits ? creep.pos.getRangeTo(a.pos) - creep.pos.getRangeTo(b.pos) : a.hits - b.hits);

                if(!creep.memory.builded){
                    targets = creep.room.find(FIND_MY_CONSTRUCTION_SITES, {
                        filter: object => object.structureType == STRUCTURE_RAMPART || object.structureType == STRUCTURE_WALL
                    });
                    targets2 = targets.concat(targets2);
                }


                if(targets2.length > 0){
                    creep.memory.assignation = targets2[0].id;
                    creep.memory.assigned = true;
                }
                else{
                    canFix = false;
                }
            }
            if(creep.memory.assigned) {
                var buildResult = creep.build(Game.getObjectById(creep.memory.assignation));
                if(buildResult == OK){
                    creep.memory.builded = true;
                    Memory.rooms[creep.memory.home].newStructure = true;
                }
                else if(buildResult == ERR_NOT_IN_RANGE) {
                    creep.moveTo(Game.getObjectById(creep.memory.assignation), {visualizePathStyle: {stroke: '#ffff00', opacity: 0.9}});
                }
                else if(creep.repair(Game.getObjectById(creep.memory.assignation)) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(Game.getObjectById(creep.memory.assignation), {visualizePathStyle: {stroke: '#ffff00', opacity: 0.9}});
                }

                if(Game.getObjectById(creep.memory.assignation).hits == Game.getObjectById(creep.memory.assignation).hitsMax){
                    creep.memory.assigned = false;
                }
            }

            if(!canFix){
                roleFixer.run(creep);
            }
	    }
	    else {
            behaviourEnergy.gatherEnergy(creep);
	    }
	}
};

module.exports = roleRampart;