const { MINER, RUNNER } = require('./constants');
const { PriorityQueue } = require('./priorityQueue');
const MINER_PRIORITY = 10;
const RUNNER_PRIORITY = 5;

var spawnManager =  {
    // create a new function for StructureSpawn
    createCustomCreep : function(energy, roleName, room, priority=0) {
            // create a balanced body as big as possible with the given energy
            var numberOfParts = Math.floor(energy / 200);
            var body = [];
            for (let i = 0; i < numberOfParts; i++) {
                body.push(WORK);
            }
            for (let i = 0; i < numberOfParts; i++) {
                body.push(CARRY);
            }
            for (let i = 0; i < numberOfParts; i++) {
                body.push(MOVE);
            }
            if(energy > 200){
                if(energy % 200 >= 150){
                    body.push(WORK);
                    body.push(CARRY);
                }
                else if(energy % 200 >= 100){
                    body.push(WORK);
                }
                else if(energy % 200 >= 50){
                    body.push(CARRY);
                }
            }
            let request = {
                body: body, 
                name: roleName + Game.time, 
                options: {memory: {role: roleName, full: false, home: room.name}}
            };
            if(!("spawnQueue" in room.memory)){
                room.memory.spawnQueue = new PriorityQueue();
            }
            let pr = new PriorityQueue(room.memory.spawnQueue);
            pr.push(request, priority);

            if(!("waitingForSpawn" in room.memory)){
                room.memory.waitingForSpawn = {};
            }
            if(!(roleName in room.memory.waitingForSpawn)){
                room.memory.waitingForSpawn[roleName] = 0;
            }
            room.memory.waitingForSpawn[roleName]++;
        },
    createLongDistanceHarvesterCreep : function(energy, roleName, targetRoom, room) {
            // create a balanced body as big as possible with the given energy
            if(energy < 400){
                return Error("NOT_ENOUGH_ENERGY_FOR_DISTANCE_HARVESTER");
            }
            var numberOfWorkParts = Math.floor(energy / 400);
            var otherParts = Math.floor((energy - (numberOfWorkParts * 150)) / 100);
            var body = [];
            for (let i = 0; i < numberOfWorkParts; i++) {
                body.push(WORK);
            }
            for (let i = 0; i < otherParts; i++) {
                body.push(CARRY);
            }
            for (let i = 0; i < (otherParts + numberOfWorkParts); i++) {
                body.push(MOVE);
            }

            let request = {
                body: body, 
                name: roleName + Game.time, 
                options: {memory: {role: roleName, full: false, home: room.name, harvestRoom: targetRoom}}
            };
            
            if(!("spawnQueue" in room.memory)){
                room.memory.spawnQueue = new PriorityQueue();
            }
            let pr = new PriorityQueue(room.memory.spawnQueue);
            pr.push(request);
            
            if(!("waitingForSpawn" in room.memory)){
                room.memory.waitingForSpawn = {};
            }
            if(!(roleName in room.memory.waitingForSpawn)){
                room.memory.waitingForSpawn[roleName] = {};
            }
            if(!(targetRoom in room.memory.waitingForSpawn[roleName])){
                room.memory.waitingForSpawn[roleName][targetRoom] = 0;
            }
            room.memory.waitingForSpawn[roleName][targetRoom]++;
        },

    createMiner : function (energy, room) {
            let body = [];
            body.push(MOVE);
            let avaibleWork = Math.floor((energy - 50) / 100);
            avaibleWork = Math.min(avaibleWork, 6);
            for(let i = 0; i < avaibleWork; i++){ //one extra since miner has to move to target
                body.push(WORK);
            }
            let leftEnergy = energy - 50 - (avaibleWork * 100);
            let extraMove = Math.floor(leftEnergy / 50);
            extraMove = Math.min(extraMove, avaibleWork-1);
            for(let i = 0; i < extraMove; i++){
                body.push(MOVE);
            }

            let request = {
                body: body, 
                name: MINER + Game.time, 
                options: {memory: {role: MINER, home: room.name}}
            };

            if(!("spawnQueue" in room.memory)){
                room.memory.spawnQueue = new PriorityQueue();
            }
            let pr = new PriorityQueue(room.memory.spawnQueue);
            pr.push(request, MINER_PRIORITY);

            if(!("waitingForSpawn" in room.memory)){
                room.memory.waitingForSpawn = {};
            }
            if(!(MINER in room.memory.waitingForSpawn)){
                room.memory.waitingForSpawn[MINER] = 0;
            }
            room.memory.waitingForSpawn[MINER]++;
        },
    createRunner : function (energy, room) {
            let body = [];
            let avaibleWork = Math.floor(energy / 100);
            for(let i = 0; i < avaibleWork; i++){
                body.push(MOVE);
            }
            for(let i = 0; i < avaibleWork; i++){
                body.push(CARRY);
            }

            let request = {
                body: body, 
                name: RUNNER + Game.time, 
                options: {memory: {role: RUNNER, home: room.name}}
            };
            
            if(!("spawnQueue" in room.memory)){
                room.memory.spawnQueue = new PriorityQueue();
            }
            let pr = new PriorityQueue(room.memory.spawnQueue);
            pr.push(request, RUNNER_PRIORITY);

            if(!("waitingForSpawn" in room.memory)){
                room.memory.waitingForSpawn = {};
            }
            if(!(RUNNER in room.memory.waitingForSpawn)){
                room.memory.waitingForSpawn[RUNNER] = 0;
            }
            room.memory.waitingForSpawn[RUNNER]++;
        },
};

module.exports = spawnManager;