var roleUpgrader = require('role.upgrader');

var behaviourHarvester = {

    /** @param {Creep} creep **/
    deploy: function(creep) {
        var targets = creep.room.find(FIND_MY_STRUCTURES, {
            filter: (structure) => {
                return ((structure.structureType == STRUCTURE_EXTENSION || 
                    (structure.structureType == STRUCTURE_TOWER && (structure.store.getUsedCapacity(RESOURCE_ENERGY) / structure.store.getCapacity(RESOURCE_ENERGY) < 0.925)) || 
                    structure.structureType == STRUCTURE_SPAWN) &&
                    structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0);
            }
        });
        var targets2 = creep.room.find(FIND_STRUCTURES, {
            filter: (structure) => {
                return (structure.structureType == STRUCTURE_STORAGE && structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0);
            }
        });
        targets.sort((a,b) => a.store.getUsedCapacity(RESOURCE_ENERGY) == b.store.getUsedCapacity(RESOURCE_ENERGY) ? creep.pos.getRangeTo(a.pos) - creep.pos.getRangeTo(b.pos) : a.store.getUsedCapacity(RESOURCE_ENERGY) - b.store.getUsedCapacity(RESOURCE_ENERGY));
        targets = targets.concat(targets2);
        if(targets.length > 0) {
            creep.memory.inactive = 2;
            if(creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(targets[0], {visualizePathStyle: {stroke: '#00ff00', opacity: 0.9}});
            }
        }
        else{
            //creep.moveTo(Game.flags.Rest.pos, {visualizePathStyle: {stroke: '#ffffff'}});
            if(creep.getActiveBodyparts(WORK) > 0){
                roleUpgrader.run(creep);
                creep.memory.inactive = 2;
            }
            else{
                targets = creep.room.find(FIND_MY_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_TOWER &&
                            structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0);
                    }
                });
                targets.sort((a,b) => creep.pos.getRangeTo(a.pos) - creep.pos.getRangeTo(b.pos));
                if(targets.length > 0) {
                    creep.memory.inactive = 2;
                    if(creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(targets[0], {visualizePathStyle: {stroke: '#00ff00', opacity: 0.9}});
                    }
                    return;
                }
                if(creep.memory.inactive == 0 && creep.store.getFreeCapacity(RESOURCE_ENERGY) > 0){
                    creep.memory.full = false;
                }
                else if(creep.memory.inactive > 0){
                    creep.memory.inactive--;
                }
                creep.moveTo(Game.flags.Rest, {visualizePathStyle: {stroke: '#00ff00', opacity: 0.9}});
            }
        }
    }
};

module.exports = behaviourHarvester;