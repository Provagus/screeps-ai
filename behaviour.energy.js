var behaviourEnergy = {

    /** @param {Creep} creep **/
    gatherEnergy: function(creep, minimalLevel=0) {
        if(creep.memory.mining){
            let source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);
            if(!source){
                creep.memory.mining = false;
            }
            if(creep.harvest(source) == ERR_NOT_IN_RANGE) {
                creep.moveTo(source, {visualizePathStyle: {stroke: '#00ff00', opacity: 0.9}});
            }
        }
        else if(creep.memory.cont){
            let containers = Game.getObjectById(creep.memory.cont);
            if(containers != null && containers.store.getUsedCapacity() > 0){
                if(containers.pos != creep.pos){
                    creep.moveTo(containers, {visualizePathStyle: {stroke: '#00e6e6', opacity: 0.9}});
                }
                creep.withdraw(containers, RESOURCE_ENERGY);
            }
            else{
                delete creep.memory.cont;
            }
        }
        else{
            var containers = creep.room.find(FIND_MY_STRUCTURES, {
                filter: s => s.structureType == STRUCTURE_STORAGE && s.store.getUsedCapacity(RESOURCE_ENERGY) >= 200 &&
                s.store.getUsedCapacity(RESOURCE_ENERGY) / (s.store.getFreeCapacity(RESOURCE_ENERGY) + s.store.getUsedCapacity(RESOURCE_ENERGY)) >= minimalLevel
            });
            if(containers.length > 0){
                let closestStored  = creep.pos.findClosestByPath(containers);
                if(closestStored.pos != creep.pos){
                    creep.moveTo(closestStored, {visualizePathStyle: {stroke: '#00e6e6', opacity: 0.9}});
                }
                creep.withdraw(closestStored, RESOURCE_ENERGY);
                creep.memory.cont = closestStored.id;
            }
            else{
                containers = creep.room.find(FIND_STRUCTURES, {
                    filter: s => s.structureType == STRUCTURE_CONTAINER && s.store.getUsedCapacity(RESOURCE_ENERGY) >= 200
                });
                if(containers.length > 0){
                    let closestStored  = creep.pos.findClosestByPath(containers);
                    if(closestStored.pos != creep.pos){
                        creep.moveTo(closestStored, {visualizePathStyle: {stroke: '#00e6e6', opacity: 0.9}});
                    }
                    creep.withdraw(closestStored, RESOURCE_ENERGY);
                    creep.memory.cont = closestStored.id;
                }
                else{
                    if(creep.getActiveBodyparts(WORK) > 0){
                        creep.memory.mining = true;
                        let source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);
                        if(creep.harvest(source) == ERR_NOT_IN_RANGE) {
                            creep.moveTo(source, {visualizePathStyle: {stroke: '#00ff00', opacity: 90}});
                        }
                    }
                }
            }
        }
    },

    /** @param {Creep} creep **/
    gatherEnergyHarvester: function(creep) {
        if(creep.memory.mining){
            let source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);
            if(creep.harvest(source) == ERR_NOT_IN_RANGE) {
                creep.moveTo(source, {visualizePathStyle: {stroke: '#00ff00', opacity: 0.9}});
            }
        }
        else if(creep.memory.cont){
            let containers = Game.getObjectById(creep.memory.cont);
            if(containers != null && containers.store.getUsedCapacity() > 0){
                if(containers.pos != creep.pos){
                    creep.moveTo(containers, {visualizePathStyle: {stroke: '#00e6e6', opacity: 0.9}});
                }
                creep.withdraw(containers, RESOURCE_ENERGY);
            }
            else{
                delete creep.memory.cont;
            }
        }
        else{
            var containers = creep.room.find(FIND_STRUCTURES, {
                filter: s => s.structureType == STRUCTURE_CONTAINER && s.store.getUsedCapacity(RESOURCE_ENERGY) > 200
            });
            if(containers.length > 0){
                let closestStored  = creep.pos.findClosestByPath(containers);
                if(closestStored.pos != creep.pos){
                    creep.moveTo(closestStored, {visualizePathStyle: {stroke: '#00e6e6', opacity: 0.9}});
                }
                creep.withdraw(closestStored, RESOURCE_ENERGY);
                creep.memory.cont = closestStored.id;
            }
            else{
                creep.memory.mining = true;
                let source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);
                if(creep.harvest(source) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(source, {visualizePathStyle: {stroke: '#00ff00', opacity: 90}});
                }
            }
        }
    },

    gatheredEnergy: function(creep) {
        creep.memory.mining = false;
        delete creep.memory.cont;
    }
};

module.exports = behaviourEnergy;