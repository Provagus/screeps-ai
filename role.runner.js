var behaviourHarvester = require('behaviour.harvester');

module.exports = {
    /** @param {Creep} creep **/
    run: function (creep) {
        if(creep.room.name != creep.memory.home){
            let exit = creep.room.findExitTo(creep.memory.home);
            creep.moveTo(creep.pos.findClosestByPath(exit),
            {visualizePathStyle: {stroke: '#00ff33', opacity: 0.9}});
            return;
        }
        if(creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0){
            creep.memory.full = false;
        }
        else if(creep.store.getUsedCapacity(RESOURCE_ENERGY) >= creep.store.getCapacity(RESOURCE_ENERGY) && creep.memory.full == false){
            creep.memory.full = true;
            creep.memory.nextWithdraw = null;
            creep.say('🚚');
        }
        if(!creep.memory.full){
            //pickup energy if took resource from ground and next to structure
            if(creep.memory.nextPickup){
                let nextPickup = Game.getObjectById(creep.memory.nextPickup);
                creep.memory.nextPickup = null;
                if(nextPickup.store.getUsedCapacity(RESOURCE_ENERGY) > 0){
                    creep.withdraw(nextPickup, RESOURCE_ENERGY);
                    return;
                }
            }
            //Emergency check for creeps with large energy buffer, to drop energy and then refuel
            if(creep.room.energyAvailable != creep.room.energyCapacityAvailable && creep.store.getUsedCapacity(RESOURCE_ENERGY) >= 300){
                creep.memory.full = true;
                behaviourHarvester.deploy(creep);
                return;
            }
            let toCarry = creep.room.find(FIND_DROPPED_RESOURCES, {
                filter: s => s.resourceType == RESOURCE_ENERGY
            });
            if(toCarry.length > 0){
                let closestDropped = creep.pos.findClosestByPath(toCarry);
                creep.moveTo(closestDropped, 
                {visualizePathStyle: {stroke: '#00ff33', opacity: 0.9}}
                );
                let pickupResult = creep.pickup(closestDropped);
                if(pickupResult == OK){
                    let pickupLocationStructures = creep.room.lookForAt(LOOK_STRUCTURES, closestDropped.pos);
                    pickupLocationStructures = _.filter(pickupLocationStructures, (structure) => structure.structureType == STRUCTURE_CONTAINER || structure.structureType == STRUCTURE_STORAGE)
                    if(pickupLocationStructures.length > 0){
                        creep.memory.nextPickup = pickupLocationStructures[0].id;
                    }
                }
            }
            else{
                toCarry = creep.room.find(FIND_TOMBSTONES,{
                    filter: s => s.store.getUsedCapacity(RESOURCE_ENERGY) > 0
                });
                if(toCarry.length > 0){
                    creep.moveTo(creep.pos.findClosestByPath(toCarry), 
                    {visualizePathStyle: {stroke: '#00ff33', opacity: 0.9}}
                    );
                    creep.withdraw(creep.pos.findClosestByPath(toCarry), RESOURCE_ENERGY);
                }
                else{
                    //Refuel spawner
                    if(creep.room.energyAvailable != creep.room.energyCapacityAvailable){
                        toCarry = creep.room.find(FIND_STRUCTURES,{
                            filter: s => (s.structureType == STRUCTURE_CONTAINER || s.structureType == STRUCTURE_STORAGE) && s.store.getUsedCapacity(RESOURCE_ENERGY) > 0
                        });
                        toCarry.sort((a,b) => a.store.getUsedCapacity() == b.store.getUsedCapacity() ? 
                            creep.pos.getRangeTo(a.pos) - creep.pos.getRangeTo(b.pos) : b.store.getUsedCapacity() - a.store.getUsedCapacity());
                        if(toCarry.length > 0){
                            creep.moveTo(creep.pos.findClosestByPath(toCarry), 
                            {visualizePathStyle: {stroke: '#00ff33', opacity: 0.9}}
                            );
                            creep.withdraw(creep.pos.findClosestByPath(toCarry), RESOURCE_ENERGY);
                        }
                    }
                    //Refuel rest
                    else{
                        if(creep.memory.nextWithdraw){
                            let sourceObject = Game.getObjectById(creep.memory.nextWithdraw);
                            if(sourceObject.store.getUsedCapacity(RESOURCE_ENERGY) > 0){
                                creep.moveTo(sourceObject, 
                                    {visualizePathStyle: {stroke: '#00ff33', opacity: 0.9}}
                                    );
                                    creep.withdraw(sourceObject, RESOURCE_ENERGY);
                                return;
                            }
                        }
                        toCarry = creep.room.find(FIND_STRUCTURES,{
                            filter: s => s.structureType == STRUCTURE_CONTAINER && s.store.getUsedCapacity(RESOURCE_ENERGY) > 0
                        });
                        toCarry.sort((a,b) => a.store.getUsedCapacity() == b.store.getUsedCapacity() ? 
                            creep.pos.getRangeTo(a.pos) - creep.pos.getRangeTo(b.pos) : b.store.getUsedCapacity() - a.store.getUsedCapacity());
                        
                        if(toCarry.length > 0){
                            toCarry = toCarry[0];
                            creep.memory.nextWithdraw = toCarry.id;
                            creep.moveTo(toCarry, 
                            {visualizePathStyle: {stroke: '#00ff33', opacity: 0.9}}
                            );
                            creep.withdraw(toCarry, RESOURCE_ENERGY);
                        }
                    }
                }
            }
        }
        else{
            behaviourHarvester.deploy(creep);
        }
    }
};