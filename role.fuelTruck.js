var roleHarvester = require('role.harvester');
var behaviourEnergy = require('behaviour.energy');

var roleFuelTruck = {

    /** @param {Creep} creep **/
    run: function(creep) {
        if(creep.memory.full && creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0) {
            creep.memory.full = false;
            creep.say('🔄');
        }
        if(!creep.memory.full && creep.store.getUsedCapacity(RESOURCE_ENERGY) == creep.store.getCapacity(RESOURCE_ENERGY)) {
            creep.memory.full = true;
            creep.say('🚛');
            behaviourEnergy.gatheredEnergy(creep);
        }
        
        if(creep.memory.full) {
            var tower = creep.room.find(FIND_MY_STRUCTURES, {
                filter: object => object.structureType == STRUCTURE_TOWER && object.store.getFreeCapacity(RESOURCE_ENERGY) > 0
            });
            tower.sort((a, b) =>
                a.store.getUsedCapacity(RESOURCE_ENERGY) == b.store.getUsedCapacity(RESOURCE_ENERGY) ? creep.pos.getRangeTo(a.pos) - creep.pos.getRangeTo(b.pos) : a.store.getUsedCapacity(RESOURCE_ENERGY) - b.store.getUsedCapacity(RESOURCE_ENERGY)
            );
            tower = tower[0];
            if(tower){
                if(creep.transfer(tower, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(tower, {visualizePathStyle: {stroke: '#000000', opacity: 0.9}});
                }
            }
            else{
                roleHarvester.run(creep);
            }
        }
        else {
            behaviourEnergy.gatherEnergy(creep, 0.5);
        }
    }
};

module.exports = roleFuelTruck;