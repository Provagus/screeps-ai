const HARVESTER = "harvester";
const BUILDER = "builder";
const UPGRADER = "upgrader";
const FIXER = "fixer";
const DEFENDER = "defender";
const RAMPART = "rampart";
const MINER = "miner";
const FUEL_TRUCK = "fuelTruck";
const RUNNER = "runner";
const LONG_DISTANCE_HARVESTER = "longHarvester";

const FRIENDS = [
    "BlkDrake"
];

module.exports.HARVESTER = HARVESTER;
module.exports.BUILDER = BUILDER;
module.exports.UPGRADER = UPGRADER;
module.exports.FIXER = FIXER;
module.exports.DEFENDER = DEFENDER;
module.exports.RAMPART = RAMPART;
module.exports.MINER = MINER;
module.exports.FUEL_TRUCK = FUEL_TRUCK;
module.exports.RUNNER = RUNNER;
module.exports.LONG_DISTANCE_HARVESTER = LONG_DISTANCE_HARVESTER;
module.exports.FRIENDS = FRIENDS;