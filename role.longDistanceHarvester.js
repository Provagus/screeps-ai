var behaviourHarvester = require('behaviour.harvester');

var roleHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {
        /*var hostiles = creep.room.find(FIND_HOSTILE_CREEPS, {
            filter: (creep) => creep.owner.username != "BlkDrake"
        });
        if(hostiles.length > 0){
            if(!Memory.hostilesToClear){
                Memory.hostilesToClear = {};
            }
            Memory.hostilesToClear[creep.room.name] = hostiles[0].ticksToLive;
        }*/
        if(creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0){
            creep.memory.full = false;
        }
        else if(creep.store.getUsedCapacity(RESOURCE_ENERGY) >= creep.store.getCapacity(RESOURCE_ENERGY) && creep.memory.full == false){
            creep.memory.full = true;
            creep.say('🚚');
        }

	    if(creep.memory.full == false) {
            if(creep.memory.harvestRoom == creep.room.name){
                var source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);
                if(creep.harvest(source) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(source, {visualizePathStyle: {stroke: '#00ff00', opacity: 0.9}});
                }
            }
            else{
                let exit = creep.room.findExitTo(creep.memory.harvestRoom);
                creep.moveTo(creep.pos.findClosestByPath(exit), {visualizePathStyle: {stroke: '#00ff00', opacity: 0.9}});
            }
        }
        else {
            if(creep.memory.home == creep.room.name){
                behaviourHarvester.deploy(creep);
            }
            else{
                let exit = creep.room.findExitTo(creep.memory.home);
                creep.moveTo(creep.pos.findClosestByPath(exit), {visualizePathStyle: {stroke: '#00ff00', opacity: 90}});
            }
        }
	}
};

module.exports = roleHarvester;