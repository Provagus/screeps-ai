module.exports = function() {
    Room.prototype.planRoom = function(){
        let availableExtensions = CONTROLLER_STRUCTURES[STRUCTURE_EXTENSION][8];
        let spawn = this.find(FIND_MY_SPAWNS);
        let used = new Set();
        let avoidArray = [];
        let containers = [];
        if(spawn.length > 0){
            spawn = spawn[0];
        }
        else{
            return;
        }
        let sources = this.find(FIND_SOURCES);
        for(let sourceName in sources){
            let source = sources[sourceName];
            let path = source.pos.findPathTo(spawn, {ignoreCreeps: true});
            if(path.length == 0){
                continue;
            }
            //this.createConstructionSite(path[0].x, path[0].y, STRUCTURE_CONTAINER);
            let i = path[0].x;
            let j = path[0].y;
            this.visual.circle(i, j, {
                fill: "#4089ff",
                radius: 0.5
            });
            used.add(`${i};${j}`);
            avoidArray.push(this.getPositionAt(i,j));
            containers.push(this.getPositionAt(i,j));
        }
        let even = (spawn.pos.x + spawn.pos.y) % 2;
        let dist = 1;
        let keepGoing = true;
        while(availableExtensions > 0 && keepGoing){
            for(let i = spawn.pos.x - dist; i <= spawn.pos.x + dist && keepGoing; i++){
                if(i <= 0 || i >= 49){
                    continue;
                }
                for(let j = spawn.pos.y - dist; j <= spawn.pos.y + dist && keepGoing; j++){
                    if(j <= 0 || j >= 49){
                        continue;
                    }
                    if(used.has(`${i};${j}`))
                        continue;
                    
                    if(this.getTerrain().get(i, j) == TERRAIN_MASK_WALL){
                        continue;
                    }
                    if(dist != 1){
                        let validP = false;
                        for(let i1 = -1; i1 <= 1; i1++){
                            for(let j1 = -1; j1 <= 1; j1++){
                                if(i1 == 0 && j1 == 0)
                                    continue;
                                if(used.has(`${i + i1};${j + j1}`)){
                                    validP = true;
                                    break;
                                }
                            }
                        }
                        if(!validP){
                            continue;
                        }
                    }
                    if((i + j) % 2 == even){
                        //extensions
                        //this.createConstructionSite(i, j, STRUCTURE_EXTENSION);
                        this.visual.circle(i, j, {
                            fill: "#6beb34"
                        });
                        used.add(`${i};${j}`);
                        avoidArray.push(this.getPositionAt(i,j));
                        availableExtensions--;
                        if(availableExtensions === 0){
                            keepGoing = false;
                            break;
                        }
                    }
                    else{
                        //road
                        //this.createConstructionSite(i, j, STRUCTURE_ROAD);
                        this.visual.circle(i, j);
                        used.add(`${i};${j}`);
                    }
                }
            }
            dist++;
        }
        for(let containerName in containers){
            let container = containers[containerName];
            let path = container.findPathTo(this.find(FIND_MY_SPAWNS)[0], 
                {
                    ignoreCreeps: true, 
                    costCallback: function(roomName, costMatrix){
                        for(let i = 0; i < avoidArray.length; i++){
                            costMatrix.set(avoidArray[i].x, avoidArray[i].y, 255);
                        }
                    }
                });
            if(path.length == 0){
                continue;
            }
            for(let i = 0; i < path.length; i++){
                let x = path[i].x;
                let y = path[i].y;
                if(used.has(`${x};${y}`))
                    continue;
                used.add(`${x};${y}`);
                this.visual.circle(x, y);
                //this.createConstructionSite(path[i].x, path[i].y, STRUCTURE_ROAD);
            }
        }

        let controller = this.controller;
        let path = controller.pos.findPathTo(spawn, 
            {
                ignoreCreeps: true, 
                costCallback: function(roomName, costMatrix){
                    for(let i = 0; i < avoidArray.length; i++){
                        costMatrix.set(avoidArray[i].x, avoidArray[i].y, 255);
                    }
                }
        });
        if(path.length != 0){
            for(let i = 0; i < path.length; i++){
                let x = path[i].x;
                let y = path[i].y;
                if(used.has(`${x};${y}`))
                    continue;
                used.add(`${x};${y}`);
                this.visual.circle(x, y);
                //this.createConstructionSite(path[i].x, path[i].y, STRUCTURE_ROAD);
            }
        }
    }
}