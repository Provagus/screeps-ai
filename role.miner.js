const { MINER } = require("./constants");

module.exports = {
    // a function to run the logic for this role
    run: function (creep) {
        // get source
        if(creep.memory.sourceId === undefined){
            let sources = creep.room.find(FIND_SOURCES);
            for (let source of sources) {
                // if the source has no miner
                if (!_.some(Game.creeps, c => c.memory.role == MINER && c.memory.sourceId == source.id)) {
                    // check whether or not the source has a container
                    /** @type {Array.StructureContainer} */
                    let containers = source.pos.findInRange(FIND_STRUCTURES, 1, {
                        filter: s => s.structureType == STRUCTURE_CONTAINER
                    });
                    // if there is a container next to the source
                    if (containers.length > 0) {
                        // spawn a miner
                        creep.memory.sourceId = source.id;
                        break;
                    }
                }
            }
            if(creep.memory.sourceId === undefined)
                creep.suicide();
        }

        let source = Game.getObjectById(creep.memory.sourceId);
        // find container next to source
        let container = source.pos.findInRange(FIND_STRUCTURES, 1, {
            filter: s => s.structureType == STRUCTURE_CONTAINER
        });
        if(container.length === 0)
            creep.suicide();
        container = container[0];

        // if creep is on top of the container
        if (creep.pos.isEqualTo(container.pos)) {
            // harvest source
            creep.harvest(source);
        }
        // if creep is not on top of the container
        else {
            // move towards it
            creep.moveTo(container, {visualizePathStyle: {stroke: '#ffed25', opacity: 0.9}});
        }
    }
};