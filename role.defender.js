var roleDefender = {

    /** @param {Creep} creep **/
    run: function(creep) {
        if(!(creep.room.controller && creep.room.controller.safeMode && !creep.room.controller.my)){
            var target = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS, {
                filter: (creep) => creep.owner.username != "BlkDrake"
            });
            var target2 = creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES, {
                filter: (object) => object.owner.username != "BlkDrake" && object.owner.username != "Source Keeper"
            });
            if(creep.pos.getRangeTo(target) > creep.pos.getRangeTo(target2)){
                target = target2;
            }
            if(target) {
                if(creep.attack(target) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target, {visualizePathStyle: {stroke: '#ff0000', opacity: 0.9}});
                }
                return;
            }
        }
        if(Game.flags.Attack){
            creep.moveTo(Game.flags.Attack.pos, {visualizePathStyle: {stroke: '#ff0000', opacity: 0.9}});
        }
        else if(Game.flags.Rest){
            creep.moveTo(Game.flags.Rest.pos, {visualizePathStyle: {stroke: '#ff0000', opacity: 0.9}});
        }
    }
};

module.exports = roleDefender;