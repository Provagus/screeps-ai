module.exports.PriorityQueue = class {
    constructor(priorityQueue=null){
        if(priorityQueue==null){
            this.priorityDict = {};
        }
        else{
            this.priorityDict = priorityQueue.priorityDict;
        }
    }
    
    push(object, priority=0){
        if(priority in this.priorityDict){
            this.priorityDict[priority].push(object);
        }
        else{
            this.priorityDict[priority] = [];
            this.priorityDict[priority].push(object);
        }
    }

    pop(){
        let keys = Object.keys(this.priorityDict);
        if(keys.length == 0){
            return undefined;
        }
        let highestPriority = keys[0];
        for(let key in keys){
            highestPriority = Math.max(highestPriority, keys[key]);
        }
        let value = this.priorityDict[highestPriority].shift();
        if(this.priorityDict[highestPriority].length == 0){
            delete this.priorityDict[highestPriority];
        }
        return value;
    }

    first(){
        let keys = Object.keys(this.priorityDict);
        if(keys.length == 0){
            return undefined;
        }
        let highestPriority = keys[0];
        for(let key in keys){
            highestPriority = Math.max(highestPriority, keys[key]);
        }
        let value = this.priorityDict[highestPriority];
        value = value[0];
        return value;
    }

    print(){
        for(let key in this.priorityDict){
            console.log(key + ": [" + this.priorityDict[key] + "]");
        }
    }
};