var roleHarvester = require('role.harvester');
var behaviourEnergy = require('behaviour.energy');

var roleBuilder = {

    /** @param {Creep} creep **/
    run: function(creep) {
	    if(creep.memory.full && creep.store.getUsedCapacity(RESOURCE_ENERGY) == 0) {
            creep.memory.full = false;
            creep.say('🔄');
	    }
	    if(!creep.memory.full && creep.store.getUsedCapacity(RESOURCE_ENERGY) >= creep.store.getCapacity(RESOURCE_ENERGY)) {
	        creep.memory.full = true;
            creep.say('🚧');
            behaviourEnergy.gatheredEnergy(creep);
	    }

	    if(creep.memory.full) {
            let targets = creep.room.find(FIND_MY_CONSTRUCTION_SITES, { 
                filter: (object) => object.structureType != STRUCTURE_ROAD });
            let targets2 = creep.room.find(FIND_MY_CONSTRUCTION_SITES, { 
                filter: (object) => object.structureType == STRUCTURE_ROAD });
            targets = targets.concat(targets2);
            if(targets.length > 0){
                if(creep.build(targets[0]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targets[0], {visualizePathStyle: {stroke: '#2302f5', opacity: 0.9}});
                }
            }
            else{
                roleHarvester.run(creep);
            }
	    }
	    else {
            behaviourEnergy.gatherEnergy(creep);
	    }
	}
};

module.exports = roleBuilder;