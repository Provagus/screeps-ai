require('prototype.spawn')();
require('prototype.room')();
const { HARVESTER, BUILDER, UPGRADER, FIXER, DEFENDER, RAMPART, MINER, FUEL_TRUCK, RUNNER, LONG_DISTANCE_HARVESTER, } = require('./constants');

global.roles = {
    [HARVESTER] : require('role.harvester'),
    [BUILDER] : require('role.builder'),
    [UPGRADER] : require('role.upgrader'),
    [FIXER] : require('role.fixer'),
    [DEFENDER] : require('role.defender'),
    [RAMPART] : require('role.rampart'),
    [MINER] : require('role.miner'),
    [FUEL_TRUCK] : require('role.fuelTruck'),
    [RUNNER] : require('role.runner'),
    [LONG_DISTANCE_HARVESTER] : require('role.longDistanceHarvester'),
};

Memory.overlay = true;

module.exports.loop = function () {
    //Cleaning dead creeps from memory
    for(let name in Memory.creeps) {
        if(!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log('Clearing non-existing creep memory:', name);
        }
    }

    for(let roomName in Game.rooms){
        let room = Game.rooms[roomName];
        room.PerformNormalTasks();
    }

    for(let spawn in Game.spawns){
        spawn = Game.spawns[spawn];
        spawn.spawnFromQueue();
    }

    //Make global array for it
    for(let name in Game.creeps) {
        try{
            var creep = Game.creeps[name];
            global.roles[creep.memory.role].run(creep);
        }
        catch(error){
            console.log(error.stack);
            Game.notify(error.stack);
        }
    }
};
