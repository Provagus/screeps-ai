const { MINER, RUNNER, LONG_DISTANCE_HARVESTER } = require('./constants');
const { PriorityQueue } = require('./priorityQueue');

module.exports = function() {

        StructureSpawn.prototype.spawnFromQueue =
        function() {
            if(!("spawnQueue" in this.room.memory)){
                return;
            }
            if(this.spawning) {
                var beingSpawned = this.spawning;
                var spawningCreep = Game.creeps[beingSpawned.name];
                this.room.visual.text(
                    '🛠️' + spawningCreep.memory.role + " (" + Math.floor((1 - beingSpawned.remainingTime / beingSpawned.needTime) * 100) + "%)",
                    this.pos.x + 1,
                    this.pos.y,
                    {align: 'left', opacity: 0.8});
                return;
            }
            let queue = new PriorityQueue(this.room.memory.spawnQueue);
            let spawnOrder = queue.first();
            if(spawnOrder === undefined){
                return;
            }
            let result = this.spawnCreep(spawnOrder.body, spawnOrder.name, spawnOrder.options);
            if(result == OK){
                queue.pop();
                let creepRole = spawnOrder.options.memory.role;
                if(creepRole == LONG_DISTANCE_HARVESTER){
                    this.room.memory.waitingForSpawn[LONG_DISTANCE_HARVESTER][spawnOrder.options.memory.harvestRoom] -= 1;
                }
                else{
                    this.room.memory.waitingForSpawn[creepRole] -= 1;
                }
            }
        };
};